BIN=mlp
PYTHON_VERSION=python3.6m
INTERFACE=libmlpsimple

CPPFILES=$(wildcard *.cpp)
HFILES=$(wildcard *.h) /usr/local/include/cefutils
DEPENDS=$(HFILES) Makefile

DBG=-O0 -D_DEBUG -g
REL=-O3 -DNDEBUG
OPTS=$(REL) -Wall -Wextra -Weffc++ -pedantic N  
LINK=-lm -ldl -lbacktrace -lboost_python3 -lboost_numpy3 -l$(PYTHON_VERSION)

#CC=clang++ $(OPTS)
#CC=g++ $(OPTS) -DBOOST_STACKTRACE_USE_BACKTRACE
CC=nvcc -std=c++11 $(REL) -I /usr/include/$(PYTHON_VERSION)  -DBOOST_STACKTRACE_USE_BACKTRACE

test: $(BIN) #testlib
	@ - rm -f out.txt 
	$(BIN) | tee out.txt
	@ (diff -dw out.txt ref.txt && tail out.txt && echo OK) || echo "DIFFS.."

testlib:  $(INTERFACE).so Makefile
	@# nm -u $(INTERFACE).so
	#/usr/bin/python3.6m test.py
	/opt/anaconda3/bin/python3 test.py

$(BIN): main.cpp $(DEPENDS) 
	@ echo "CC [$< -> $@]"
	@ $(CC) main.cpp -o $(BIN) $(LINK)

test: $(BIN) 

$(INTERFACE).so: $(INTERFACE).cpp $(DEPENDS)
	@ echo "CC [$< -> $@]"
	@ $(CC) -I /usr/include/$(PYTHON_VERSION) $(INTERFACE).cpp -o $(INTERFACE).so -fPIC -shared $(LINK) -lboost_python3 -lboost_numpy3 -l$(PYTHON_VERSION)
	 
#.o: .cpp
#   @ echo "CC [$< -> $@]"
#   @ $(CC) -fpic -c $< -o $@ $(LINK)
#   @ echo "CC [$< -> $@] DONE"
#
#$(BIN): main.o $(DEPENDS) #$(INTERFACE).o
#   @ echo "CC [$< -> $@]"
#   $(CC) $< main.o -o $@ $(LINK) 

apt:
	sudo apt install libboost-python-dev

clean:
	@ - rm -f out.txt $(BIN) $(INTERFACE).so
