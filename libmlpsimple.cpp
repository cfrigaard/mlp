#include <boost/python.hpp>
#include <boost/python/numpy.hpp>

#include "mlp.h"

#define PY_ERR(msg) PyHelpers::PyError(CefUtils::tostring(msg), CefUtils::Srcdbg(__LINE__,__FILE__,__FUNCTION__))

namespace  p = boost::python;
namespace np = boost::python::numpy;

using std::string;
using CefUtils::tostring;

namespace PyHelpers
{
	void PyError(const string msg, CefUtils::Srcdbg dbg)
	{
		PyErr_SetString(PyExc_IndexError, msg.c_str());
		CefUtils::Err(msg, dbg);
		//p::throw_error_already_set();
	}

	template<typename T> void assertsize(const int expected_size, const char*const typemsg)
	{
		if (sizeof(T)!=expected_size) {
			PY_ERR("expected c++ sizeof(" + string(typemsg) + ")==" + tostring(expected_size) + ", but got " + tostring(sizeof(T)));
		}
	}

	bool _Init()
	{
		assertsize<char>  (1, "char");
		assertsize<int>   (4, "int");
		assertsize<size_t>(8, "size_t");
		assertsize<float> (4, "float");
		assertsize<double>(8, "double");

		Py_Initialize();
		np::initialize();
		return true;
	}

	static bool _init=_Init();
}

class NpArr
{
public:
	//typedef MLP::t_flt t_flt;
	typedef const t_flt*const* t_ppflt;

private:
	const np::ndarray& _arr;

	/*
	static int g_int(p::object x)
	{
		p::extract<int> get_int(x);
		if (get_int.check()) return get_int();
		PY_ERR("wrong type in python object");
		return 0;
	}
	*/

	static t_flt flt(p::object x)
	{
		p::extract<t_flt> get_float(x);
		if (get_float.check()) {
			const t_flt t=get_float();
			return t;
		}
		PY_ERR("wrong type in python object");
		return 0;
	}

public:
	NpArr(const np::ndarray& arr, const int expected_dim=2) : _arr(arr)
	{
		assert(sizeof(t_flt)==sizeof(float));
		CheckArray<t_flt>(_arr, expected_dim, "float32");
		assert(expected_dim==nd());
	}

	int nd() const
	{
		return _arr.get_nd();
	}

	int shape(const int i) const
	{
		if(i<0 || i>1) PY_ERR("shape i==" + tostring(i) + " out-of-range");
		return _arr.shape(i);
	}

	t_ppflt toflt() const
	{
		//t_ppflt d=reinterpret_cast<t_ppflt>(_arr.get_data());
		//assert(d);
		const int m=shape(0), n=shape(1);
		float** pp=new float*[m];
		for(int i=0; i<m; ++i) {
			pp[i] = new float[n];
			for(int j=0; j<n; ++j) {
				pp[i][j]=(*this)(i, j);
			}
		}
  		return pp;
  	}

  	t_flt operator()(const int i, const int j) const
	{
		assert(i<shape(0));
		assert(j<shape(1));
		return flt(_arr[i][j]);
	}
	
	//const t_flt* operator[](const int i) const
	//{
	//	assert(false);
	//	return 0;
	//}	

	friend std::ostream& operator<<(std::ostream& s, const NpArr& x)
	{
  		s << "NpArr(ndim=" + tostring(x.nd()) + ", shape=[" << tostring(x.shape(0)) + ";" + tostring(x.shape(1)) + "]: [";
  		for(int i=0; i<x.shape(0); ++i) {
  			s << "\n\t[";
  			for(int j=0; j<x.shape(1); ++j) {
 				//s << tostring(pp[i][j]);
 				if (j>0) s << ",\t";
 				//s << p::extract<char const *>(p::str(x._arr[i][j]));
 				s << x(i, j);
			}
 			s << "]";
		}
		return s << "\n]";
	}

	template<typename T> static const T* CheckArray(const np::ndarray& arr, const int expected_dim, const string expected_type)
	{
		const int expected_itemsize=sizeof(T);
		//cout << "CheckArray(arr ,expected_dim=" << expected_dim << ", expected_itemsize=" << expected_itemsize << ", expected_type=" << expected_type << ")..";

		const int                  dim    = arr.get_nd();
		const np::ndarray::bitflag b      = arr.get_flags();
		const np::dtype            typ    = arr.get_dtype();
		const char*const           typmsg = p::extract<char const *>(p::str(typ));
		const int                  shape0 = arr.shape(0);
		const int                  shape1 = arr.shape(1);
		//const int                  stride0= arr.shape(0);
		//const int                  stride1= arr.shape(1);

		if (dim!=expected_dim)                     PY_ERR("wrong dimension in size array, expected dim==" + tostring(expected_dim) + ", got dim=" + tostring(dim));
		if (!(b & np::ndarray::C_CONTIGUOUS))      PY_ERR("expected C_CONTIGUOUS ndarray");
		if (!(b & np::ndarray::V_CONTIGUOUS))      PY_ERR("expected V_CONTIGUOUS ndarray");
		//if ( (b & np::ndarray::F_CONTIGUOUS))      PY_ERR("expected !F_CONTIGUOUS ndarray"); // fortran contiguous
		if (typ.get_itemsize()!=expected_itemsize) PY_ERR("expected itemsize==" + tostring(expected_itemsize) + ", got itemsize=" + tostring(typ.get_itemsize()));
		if (string(typmsg)!=expected_type)         PY_ERR("expected type=='" + expected_type + "' but got type='" + string(typmsg) + "'");
		if (shape0<0)                              PY_ERR("expected shape[0]>0, but got shape[0]=" + tostring(shape0));
		if (shape1<0)                              PY_ERR("expected shape[1]>0, but got shape[1]=" + tostring(shape1));

		//const int t=s.strides(0);
		//if (t!=0) PY_ERR("can not handle anything but stride==0, got stride=" + tostring(t));

		const T*const d=reinterpret_cast<T*>(arr.get_data());
		//cout << "OK" << endl;

		return d;
	}
};

class mlpsimple
{
private:
	MLP<>* _model;

	std::pair<NpArr::t_ppflt, NpArr::t_ppflt> Arrs(const np::ndarray X, const np::ndarray y)
	{
		if (!_model) PY_ERR("model not initialized");

		const NpArr arrX(X), arry(y);
		//cout << "X=" << arrX << endl;
		//cout << "y=" << arry << endl;

		if (arrX.shape(0)!=arry.shape(0)) PY_ERR("mismatch in X,y shapes");
		return std::make_pair(arrX.toflt(), arry.toflt());
	}

public:
    mlpsimple(const np::ndarray layer_sizes, double learning_rate, const int mode_lr, double alpha, int num_iter, double threshold) : _model(0)
	{
		const int*const d=NpArr::CheckArray<int>(layer_sizes, 1, "int32");
		const int nl=layer_sizes.shape(0);

		_model=new MLP<>(nl, d, learning_rate, mode_lr, alpha, num_iter, threshold);
    }

    void fit(const np::ndarray X, const np::ndarray y)
    {
    	cout << "fit..X.shape=(" << X.shape(0) << ";" << X.shape(1) << ")" <<  endl;
		std::pair<NpArr::t_ppflt,NpArr::t_ppflt> d=Arrs(X, y);
		
		_model->fit(X.shape(0), X.shape(1), d.first, d.second);
    }

    np::ndarray predict(const np::ndarray X)
    {
    	cout << "predict.." << endl;
		if (!_model)            PY_ERR("model not initialized");
		if (!_model->Trained()) PY_ERR("model not trained");

		const NpArr arrX(X);
		np::ndarray y(X);
		
		_model->predict(arrX.shape(0), arrX.shape(1), arrX.toflt(), y);
		//cout << "y=" << NpArr(y) << endl;
		return y;
    }

    t_flt score(const np::ndarray X, const np::ndarray y)
    {
    	cout << "score.." << endl;
		if (!_model->Trained()) PY_ERR("model not trained");
		std::pair<NpArr::t_ppflt, NpArr::t_ppflt> d=Arrs(X, y);
		
		return _model->score(X.shape(0), X.shape(1), d.first, d.second);
	}
};

BOOST_PYTHON_MODULE(libmlpsimple)
{
    p::class_<mlpsimple>("mlpsimple", p::init<const np::ndarray, const double, const int, const double, const int, const double>())
        .def("fit",    &mlpsimple::fit)
        .def("predict",&mlpsimple::predict)
        .def("score"  ,&mlpsimple::score)
	;
}