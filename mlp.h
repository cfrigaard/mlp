// BASED on: https://www.codeproject.com/Articles/13582/Back-propagation-Neural-Net

//////////////////////////////////////////////
// Fully connected multilayered feed		//
// forward	artificial neural network using	//
// Backpropogation	algorithm for training.	//
//////////////////////////////////////////////

#include <algorithm>
#include <cmath>
#include <cassert>
#include <iostream>
#include <limits>
#include <random>

#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <boost/format.hpp>
#include <cefutils>

using std::cout;
using std::endl;
using std::string;
using CefUtils::tostring;

#include "helpers.h"
#include "containers.h"
using namespace Helpers;

typedef float t_flt;

namespace weight_initializers
{
	struct He
	{
		static t_flt Generate(const int layer_size)
		{
			//w=np.random.randn(layer_size[l],layer_size[l-1])*np.sqrt(2/layer_size[l-1])
			assert(layer_size>0);
			static std::normal_distribution<t_flt> N;
			static std::default_random_engine generator(42);
			return N(generator)*std::sqrt(static_cast<t_flt>(2)/layer_size);
		}
			
		static string name() 
		{
			return "He";
		}
	};

	struct Random
	{
		static t_flt Generate(const int) // in range [-1; 1]
		{
			return RandomGen<t_flt>();
		}
			
		static string name() 
		{
			return "Random";
		}
	};
}

namespace activation_functions
{
	struct Sigmoid
	{
		static t_flt Activation(const t_flt x)
		{
			// sigmoid function
			// d/dx f(x) = f(x)*(1-f(x)) = f(x)*f(-x), with f(x)=1/(1+exp(-x))
			return AssertOk(1/(1+std::exp(-x)));
		}

		static t_flt DActivation(const t_flt f_x) 
		{
			return AssertOk(f_x*(1-f_x));
		}
		
		static string name() 
		{
			return "Sigmoid";
		}
	};

	struct ELU
	{
		static t_flt Activation(const t_flt x)
		{
			return   x>0 ? x : 1*(std::exp(x)-1);
		}

		static t_flt DActivation(const t_flt f_x) 
		{
			return f_x>0 ? 1 : 1+f_x;
		}
		
		static string name() 
		{
			return "ELU";
		}
	};
}

namespace dropouts
{
	struct None
	{
		bool operator()() const
		{
			return false;
		}

		static string name() 
		{
			return "None";
		}
	};
	
	struct Random
	{
		const t_flt _p;
		
		Random(const t_flt p) : _p(p)
		{
			assert(p>=0 && p<=1);
		}
		
		bool operator()() const
		{
			const t_flt p=RandomGen<t_flt>()+1/2;
			return p < _p;
		}

		static string name() 
		{
			return "None";
		}
	};
}
	

template<typename activation_function=activation_functions::ELU, typename weight_initialization=weight_initializers::Random>
class MLP
{
public:

private:
	const int      _layers;                // no of layers in net including input layer
	const t_flt    _learning_rate, _alpha; // lr=learning rate, alpha=momentum parameter
	const int      _num_iter;
	const t_flt    _threshold;
	const arr<int> _layer_sizes;           // vector of numl elements for size of each layer
	
	neurons<t_flt> _out;
	neurons<t_flt> _delta;                 // delta error value for each neuron
	weights<t_flt> _weights;               // vector of weights for each neuron
	weights<t_flt> _prevDwt;               // storage for weight-change made in previous epoch
	
	const int _mode_learning_rate;
	bool     _trained;

	/*
	static t_flt Sigmoid(const t_flt x)
	{
		// sigmoid function
		// d/dx f(x) = f(x)*(1-f(x)) = f(x)*f(-x), with f(x)=1/(1+exp(-x))
		return AssertOk(1/(1+std::exp(-x)));
	}

	static t_flt DSigmoid  (const t_flt f_x) {return AssertOk(f_x*(1-f_x));}
	static t_flt ReLU      (const t_flt x  ) {return   x>0 ? x   : 0.0;}
	static t_flt DReLU     (const t_flt f_x) {return f_x>0 ? 1.0 : 0.0;}		
	static t_flt LeakyReLU (const t_flt x )  {return   x>0 ? x   : 0.01*x;}
	static t_flt DLeakyReLU(const t_flt f_x) {return f_x>0 ? 1.0 : 0.01;}
	static t_flt ELU       (const t_flt x)   {return   x>0 ? x   : 1*(std::exp(x)-1);}
	static t_flt DELU      (const t_flt f_x) {return f_x>0 ? 1   : 1+f_x;}
	static t_flt TanH      (const t_flt x)   {const t_flt a=std::exp(x), b=std::exp(-x); return (a-b)/(a+b);}
	static t_flt DTanH     (const t_flt f_x) {return 1-f_x*f_x;}

	static t_flt Activation(const t_flt x)
	{	
		//return Sigmoid(x);
		//return ReLU(x);
		//return LeakyReLU(x);
		return ELU(x);
		//return TanH(x);
	}
	
	static t_flt DActivation(const t_flt f_x)
	{
		//return DSigmoid(f_x);
		//return DReLU(f_x);
		//return DLeakyReLU(f_x);
		return DELU(f_x);
		//return DTanH(f_x);
	}
	*/
	
	void AssignWeights()
	{
		//cout << "AssignWeights.." << endl << _weights;
		for(int i=1; i<_layers; ++i) {
			for(int j=0; j<_layer_sizes[i]; ++j) { 
				for(int k=0; k<_layer_sizes[i-1]+1; ++k) {
					//cout << "i=" << i << " j=" << j << " k=" << k << endl;
					_weights[i][j][k] = weight_initialization::Generate(_layer_sizes[i-1]); // Rand();
					_prevDwt[i][j][k] = 0; // initialize previous weights to 0 for first iteration
				}
			}
		}
	}

	static t_flt LearningRate(const t_flt lr0, const int iteration, const int mode_learning_rate)
	{
		t_flt lrn=-1;
		//if (iteration<4) return 0.01;
		switch(mode_learning_rate) {
			case 0: 
				lrn=lr0; 
				break;
			case 1: {
				const t_flt d=0.5, r=10000;
				const int e=(1+iteration)/r;
				lrn=lr0 * std::pow(d, e);
				break;
			}
			case 2: { 
				const t_flt d=1/100000.0f;
				lrn=lr0 * std::exp(-d*iteration);
				break;
			}
			default:  ERR("bad learning rate mode, expected mode_lr=0 (constant)/1(step-based adaptive/2(exponential)");
		}
		
		//cout << "iteration=" << iteration << ", lr0=" << lr0 << ", e=" << e << ", d=" << d << ", std::pow(d, e)=" << std::pow(d, e) << ", lrn=" << lrn << endl;
		assert(lrn>=0);
		return AssertOk(lrn);
	}

	// backpropogate errors from output layer uptill the first hidden layer
	template<typename T1, typename T2> void Backpropagate(const T1 x,const T2 y, const t_flt learning_rate)
	{		
		assert(learning_rate>0);
		int i;
		const size_t M=Neurons();
		
		predict(x); // update output values for each neuron

		// find delta for output layer
		for(i=0; i<_layer_sizes[_layers-1]; ++i) {
			const t_flt t=_out[_layers-1][i];
			_delta[_layers-1][i] = activation_function::DActivation(t) * (y[i]-t); //t * (1-t) * (y[i]-t);
		}

		// find delta for hidden layers	
		for(i=_layers-2; i>0; --i) {
			for(int j=0; j<_layer_sizes[i]; ++j) {
				t_flt sum=0.0;
				for(int k=0; k<_layer_sizes[i+1]; ++k) {
					sum += _delta[i+1][k] * _weights[i+1][k][j];
				}
				const t_flt t=_out[i][j];
				_delta[i][j] = activation_function::DActivation(t) * sum; // t * (1-t) * sum;
			}
		}

		// apply momentum (does nothing if alpha=0)
		if (_alpha>0) {
			for(i=1; i<_layers; ++i) {
				for(int j=0; j<_layer_sizes[i]; ++j) {
					for(int k=0; k<_layer_sizes[i-1]; ++k) {
						_weights[i][j][k] += _alpha * _prevDwt[i][j][k];
					}
					_weights[i][j][_layer_sizes[i-1]] += _alpha * _prevDwt[i][j][_layer_sizes[i-1]];
				}
			}
		}
		
		// adjust weights using steepest descent
		//const t_flt aa=0.1;		
		for(i=1; i<_layers; ++i) {
			for(int j=0; j<_layer_sizes[i]; ++j) {
				for(int k=0; k<_layer_sizes[i-1]; ++k) {
					_prevDwt[i][j][k]  =  learning_rate *_delta[i][j] * _out[i-1][k]; // + aa*_weights[i][j][k]; //weight decay?
					_weights[i][j][k] += _prevDwt[i][j][k];
				}
				_prevDwt[i][j][_layer_sizes[i-1]]  = learning_rate *_delta[i][j]; // bias
				_weights[i][j][_layer_sizes[i-1]] += _prevDwt[i][j][_layer_sizes[i-1]];
			}
		}
	}

	template<typename T> void AssertSizes(const int M, const int N, const T X) const
	{
		(void)X;
		(void)M;
		assert(M>0 && N>0);
		if (N!=_layer_sizes[0]) ERR("size N=" + tostring(N) +" must match layer(0) size=" + tostring(_layer_sizes[0]));
		//dump(X, m, n, _layer_sizes[0], "X");
		//dump(y, m, n, _layer_sizes[_layers-1], "y");
	}
	
	MLP(const MLP&);
	void operator=(const MLP&);

public:
	// initializes and allocates memory on heap
	MLP(const int layers, const int*const layer_sizes, const t_flt learning_rate, const int mode_learning_rate, const t_flt alpha, const int num_iter, const t_flt threshold) :
	_layers            (layers),
	_learning_rate     (learning_rate),
	_alpha             (alpha),
	_num_iter          (num_iter),
	_threshold         (threshold),
	_layer_sizes       (_layers, layer_sizes, "layer_sizes"), 
	_out               (_layers, layer_sizes),
	_delta             (_layers, layer_sizes),
	_weights           (_layers, layer_sizes), 
	_prevDwt           (_layers, layer_sizes),
	_mode_learning_rate(mode_learning_rate),
	_trained  (false)
	{
		assert(_layers>0);
		assert(_learning_rate>=0);
		assert(_alpha>=0);
		assert(_num_iter>=0);
		assert(_threshold>=0);
			
		// srand((unsigned)(time(NULL)));
		srand(42);
		AssignWeights();

		// Note that the following variables are unused,
		//
		// _delta[0]
		// _weights[0]
		// _prevDwt[0]

		//  I did this intentionaly to maintains consistancy in numbering the layers.
		//  Since for a net having n layers, input layer is refered to as 0th layer,
		//  first hidden layer as 1st layer and the nth layer as output layer. And
		//  first (0th) layer just stores the inputs hence there is no delta or weight
		//  values corresponding to it.
	}

	~MLP()
	{
	}

	// returns i'th output of the net
	t_flt Out(const int i) const
	{
		assert(i<_layer_sizes[_layers-1]);
		return _out[_layers-1][i];
	}

	bool Trained() const
	{
		return _trained;
	}

	size_t Parameters() const
	{
		size_t n=0;
		for(int i=1; i<_weights.size(); ++i) {
			for(unsigned int j=0; j<_weights[i].size(); ++j) n += _weights[i][j].size();
		}
		return n;
	}

	size_t Neurons() const
	{
		size_t n=0;
		for(int i=1; i<_out.size(); ++i) n +=_out[i].size();
		return n;
	}
	
	size_t Memoryusage() const 
	{
		return _layer_sizes.memusage() + _out.memusage() + _delta.memusage() + _weights.memusage() + _prevDwt.memusage();
	}

	template<typename T1, typename T2> t_flt fit(const int M, const int N, const T1 X, const T2 y)
	{	
		AssertSizes(M, N, X);
		//dump(X, m, n, _layer_sizes[0], "X");
		//dump(y, m, n, _layer_sizes[_layers-1], "y");		

		arr<int> shuffle(M);
		int epoch=0, curri=0, iteration;
		
		for(iteration=0; iteration<_num_iter; ++iteration)
		{
			if (curri>=M) {
				curri=0;
				++epoch;
			}
			if (curri==0) {
				if (epoch==0) for(int j=0; j<M; ++j) shuffle[j]=j;
				std::random_shuffle(shuffle.begin(), shuffle.end());
			}	
			
			assert(curri>=0 && curri<M);	
			const int i=shuffle[curri++];
			//const int i=static_cast<int>((Rand()+1)/2*M); // i=_iteration%M;
			assert(i>=0 && i<M);
			
			const t_flt lr=LearningRate(_learning_rate, iteration, _mode_learning_rate);
			if (lr<=1E-30)  {
				cout << "  learning rate=0, breaking.." << endl;
				break;
			}
			
			Backpropagate(X[i], y[i], lr);
			
			if (iteration%1000==0) {
				const t_flt s=score(M, N, X, y);
				cout << "  [" << epoch << "/" << iteration << "]: score:  " << boost::format("%3.8f") % s << ", learning_rate=" <<  LearningRate(_learning_rate, iteration, _mode_learning_rate) << " .." << endl;
				if (s<_threshold) {
					cout << "  score above threshold=" << _threshold << ", breaking.." << endl;
					break;
				}
			}
		}
	
		cout << "DONE: final score=" << score(M, N, X, y) << " in " << iteration << " iterations." << endl;
		_trained=true;
		
		return 0;
	}

	// feed forward one set of input
	template<typename T> void predict(const T x)
	{
		// assign content to input layer
		for(int i=0; i<_layer_sizes[0]; ++i) _out[0][i] = x[i];  // output_from_neuron(i,j) Jth neuron in Ith Layer

		// assign output(activation) value to each neuron using activation func
		for(int i=1; i<_layers; ++i) {                            // For each layer
			for(int j=0; j<_layer_sizes[i]; ++j) {               // For each neuron in current layer
				t_flt sum=0.0;
				for(int k=0; k<_layer_sizes[i-1]; ++k) {         // For input from each neuron in preceeding layer
					sum += _out[i-1][k]*_weights[i][j][k];  // Apply weight to inputs and add to sum
				}
				sum += _weights[i][j][_layer_sizes[i-1]];        // Apply bias is important
				_out[i][j] = activation_function::Activation(sum);               // Apply activation function
			}
		}
	}

	template<typename T> void predict(const int M, const int N, const T X, boost::python::numpy::ndarray& y_pred)
	{
		AssertSizes(M, N, X);
		for(int i=0; i<M; ++i) {
			predict(X[i]);
			for(int j=0; j<_layer_sizes[_layers-1]; ++j) {
				y_pred[i][j]=Out(j);
			}
		}
	}
	
	template<typename T1, typename T2> t_flt score(const int M, const int N, const T1 X, const T2 y)
	{
		AssertSizes(M, N, X);
		t_flt mse=0;
		for(int i=0; i<M; ++i) {
			predict(X[i]);
			for(int j=0; j<_layer_sizes[_layers-1]; ++j) {
				const t_flt d=y[i][j]-Out(j);
				mse += AssertOk(d*d);
			}
		}	
		return AssertOk(mse/2/M);
	}
	
	void printweights() const 
	{	
		const weights<t_flt>& x=_weights; //_prevDwt;// _weights;
		for(int i=1; i<_layers; ++i) {
			cout << "layer=" << fill(i, 4) << endl;
			for(int j=0; j<_layer_sizes[i]; ++j) {
				const auto   t0=boost::format("%+5f") % _out[i][j];
				const string sj="j=" + fill(j,4) + "  o=" + fill(t0,6);
				cout << sj;
				for(int k=0; k<_layer_sizes[i-1]; ++k) {
					if (k>0) cout << fill("", sj.size()); 
					const auto t1=boost::format("%+5f") % (x[i][j][k]);
					cout << " " << t1;
				}
				cout << endl;
			}
			cout << endl;
		}
		cout << endl;
	}
		
	friend std::ostream& operator<<(std::ostream& s, const MLP& x)
	{
		s << "MLP::MLP<" << activation_function::name() << ", " << weight_initialization::name() << ">(" << "layers=" << x._layers << ", layer_sizes={";
		for(int i=0; i<x._layers; ++i) {
			if (i>0) s << ", ";
			s << x._layer_sizes[i];
		}
		s << "}, learning_rate=" << x._learning_rate << ", alpha=" << x._alpha;
		s << ", num_iter=" << x._num_iter << ", threshold=" << x._threshold << ")";
	
		const size_t m=x.Memoryusage();	
		s << ", neurons=" << x.Neurons() << ", parameters=" << x.Parameters() << ", mem=" << x.Memoryusage() << " B" << HumanReadable(m) << ", trained=" << (x._trained ? "yes" : "no");
		return s;
	}	
};
