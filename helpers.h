namespace Helpers
{
	template<typename T> static T RandomGen() // in range [-1; 1]
    {
        //const t_flt r=static_cast<t_flt>(rand())/(RAND_MAX/2) - 1;
        //assert(r>=-1 && r<=1);
        //return r;
        const T r=static_cast<T>(rand())/(RAND_MAX);
        assert(r>=0 && r<=1);
        return r;
    }

	template<typename T> string fill(const T& s, const int len)
	{
		string t=tostring(s), r;
		while(t.size()+r.size()<static_cast<size_t>(len)) r += " ";
		return r+t;
	}
	
	#ifdef _DEBUG
		template<typename T> static std::pair<bool, string> Ok(const T x, const T fmax=1E200, const T fmin=1E-30) // denorm min for float=1.17549e-38
		{	
			string msg;
			if ( x!=x)                                    return std::make_pair(false, "x is NAN");
			if ( x==std::numeric_limits<T>::infinity())   msg += "x is inf ";
			if (-x==std::numeric_limits<T>::infinity())   msg += "x is -inf ";
			if ( x==std::numeric_limits<T>::denorm_min()) msg += "x is denorm ";
			if (-x==std::numeric_limits<T>::denorm_min()) msg += "x is denorm ";
			if ( x>fmax)                                  msg += "x>fmax ";
			if (-x>fmax)                                  msg += "x>-fmax ";
			if ( x>0 &&  x<fmin)                          msg += "x<fmin ";
			if ( x<0 && -x<fmin)                          msg += "x<-fmin ";
			
			bool ok=msg=="";
			return std::make_pair(ok, msg);
		}
		
		template<typename T>
		static T AssertOk(const T x)
		{
			const std::pair<bool, string> ok=Ok(x);
			if (!ok.first) ERR("t_flt not ok, " + ok.second);
			return x;
		}
		
		template<typename T> static void dump(const T A, const int M, const int N, const string& msg)
		{
			cout << msg << "(M=" << M << "; N=" << N << "):" << endl << "[" << endl; 
			for(int i=0; i<M; ++i) { 
				cout << "  [";
				for(int j=0; j<N; ++j) {
					const T t=A[i][j];
					AssertOk<T>(t);
					if (j>0) std::cout << ", ";
					std::cout << t;
				}
				cout << "]" << endl;
			}
			cout << "]" << endl;
		}	
	#else
		#define AssertOk(x) x
	#endif
	
	string HumanReadable(const size_t m)
	{
	    string msg="=";
        if (m>1024*1024*1024) msg += tostring(m/1024/1024/1024) + " GB";
        else if (m>1024*1024) msg += tostring(m/1024/1024)      + " MB";
        else if (m>1024)      msg += tostring(m/1024)           + " KB";
        else msg = "";
        return msg;
	}	
	
	/*
	class Memhandler
	{
	private:
		static size_t _allocated;
			
	public:	
		template<typename T> static T* alloc(const int n)
		{
			assert(n>0);
			T* t=new T[n];
			if (!t) ERR("allocation returned null pointer");
			_allocated += n*sizeof(T);
			return t;
		}

		template<typename T> static void del1D(T*const& t, const int n)
		{
			assert(t);
			delete[] t;
			_allocated -= n*sizeof(t);
		}
		
		static  size_t allocated()
		{
			return _allocated;
		}
	};
	
	size_t MLP::Memhandler::_allocated=0; // NOTE: should be in cpp file
	*/	
}
