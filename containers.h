template<typename T> class arr
{
private:
	T*const _dat;
	const int _sz;
	#ifdef _DEBUG
		const char*const _msg;
	#endif

	T* alloc(const int sz)
	{
		assert(sz>=0);
		T*const t=new T[sz];
		if (!t) ERR("allocation error, array allocation of" + tostring(sz) + " bytes returned null pointer");
		return t;
	}
	
	template<typename R> T* alloc(const int sz, const R r)
	{
		T*const t=alloc(sz);		
		for(int i=0; i<sz; ++i) {
			const T s=r[i];
			assert(s>0);
			t[i]=s;
		}
		return t;
	}

	arr<T>(const arr<T>&);
	void operator=(const arr<T>&);
	
public:
	arr(const int sz, const char*const msg=0) : 
		_dat(alloc(sz)), _sz(sz)
		#ifdef _DEBUG
			,_msg(msg)
		#endif
	{
		#ifdef NDEBUG
			(void)msg;
		#endif
	}

	template<typename R> arr(const int sz,const R r, const char*const msg=0) : 
		_dat(alloc(sz, r)), _sz(sz)
		#ifdef _DEBUG
			,_msg(msg)
		#endif
	{
		#ifdef NDEBUG
			(void)msg;
		#endif
	}
	
	~arr() 
	{	
		assert(_dat);
		delete[] _dat;
	}	

	T& operator[](const int n) 
	{
		if(!(n>=0 && n<_sz)) ERR("n=" + tostring(n) + " out-of-range, _sz=" + tostring(_sz));
		assert(n>=0 && n<_sz);
		return _dat[n];
	}
	
	const T& operator[](const int n) const 
	{
		assert(n>=0 && n<_sz);
		return _dat[n];
	}
		
	T*       begin   ()       {return _dat;}
	T*       end     ()       {return _dat+_sz;}
	const T* begin   () const {return _dat;}
	const T* end     () const {return _dat+_sz;}		
	int      size    () const {return _sz;}
	size_t   memusage() const {return _sz*sizeof(T);}
	
	#ifdef _DEBUG
		const char* Msg() const {return _msg;}
	#endif
	
	friend std::ostream& operator<<(std::ostream& s, const arr& lhs)
	{
		s << "arr(size=" << lhs.size() << "): ";
		int l=0;
		for(auto i: lhs) {
			s << "  [" << Helpers::fill(l++,3) << "]:" << Helpers::fill(i, 12);
		}
		return s;
	}
};

template<typename T> class neurons
{
private:
	typedef arr<T> t_elem;
	arr<t_elem*> _dat;

public:
	neurons(const int layers, const int*const layer_sizes)
	: _dat(layers)
	{
		for(int i=0; i<layers; ++i) _dat[i]=new t_elem(layer_sizes[i]);
	}
	
	~neurons()
	{
		for(auto& i: _dat) {
			assert(i);
			delete i;
			i=0;
		}
	}	
	
	int size() const 
	{
		return _dat.size();
	}

	size_t memusage()  const 
	{
		size_t s=size()*sizeof(t_elem*);		
		for(auto i: _dat) s += i->size();
		return s;
	}

	t_elem& operator[](const int n) 
	{
		t_elem*const p=_dat[n];
		assert(p);
		return *p;
	}
	
	const t_elem& operator[](const int n) const 
	{
		const t_elem*const p=_dat[n];
		assert(p);
		return *p;
	}
	

	friend std::ostream& operator<<(std::ostream& s, const neurons& lhs)
	{
		s << "neurons, layers=" << lhs.size();
		int l=0;
		for(auto i: lhs._dat) {
			s << "  layer[" << l++ << "], size=" << i->size() << *i;
		}
		return s;
	}
};

template<typename T> class weights
{
private:
	typedef arr<T> t_elem0;
	typedef arr<t_elem0*> t_elem1;
	arr<t_elem1*> _dat;

public:
	weights(const int layers, const int*const layer_sizes)
	: _dat(layers)
	{
		_dat[0]=0;
		for(int i=1; i<layers; ++i) {
			_dat[i]=new t_elem1(layer_sizes[i]);
			for(int j=0; j<layer_sizes[i]; ++j) {
				(*_dat[i])[j]=new t_elem0(layer_sizes[i-1]+1);
			}
		}
	}
	
	~weights()
	{
		assert(_dat[0]==0);
		for(int i=1; i<_dat.size(); ++i) {
			assert(_dat[i]);
			for(int j=0; j<_dat[i]->size(); ++j) {
				assert((*_dat[i])[j]);
				delete (*_dat[i])[j];
			}
			delete _dat[i];
			_dat[i]=0;
		}
	}	
	
	int size() const 
	{
		return _dat.size();
	}

	size_t memusage()  const 
	{
		size_t s=size()*sizeof(t_elem1*);		
		for(int i=1; i<_dat.size(); ++i) {
			s += _dat[i]->size()*sizeof(t_elem0);
			for(int j=0; j<_dat[i]->size(); ++j) {
				s += (*_dat[i])[j]->size()*sizeof(T);
			}
		}
		return s;
	}
	
    template<typename R0, typename R1> class temparr
	{
		private:
			R1*const _t;
			
		public:
			temparr(R1*const t) : _t(t) 
			{
				assert(t);
			}
			
			R0& operator[](const int j)
			{
				R0* p=(*_t)[j];
				assert(p);
				return *p;
			}
			
			size_t size() const
			{
				return _t->size();
			}
	};

	temparr<t_elem0, t_elem1> operator[](const int i)
	{
		assert(i>0);
		t_elem1* p=_dat[i];
		return temparr<t_elem0, t_elem1>(p);
	}

	temparr<const t_elem0, const t_elem1> operator[](const int i) const
	{
		assert(i>0);
		const t_elem1* p=_dat[i];
		return temparr<const t_elem0, const t_elem1>(p);
	}

	friend std::ostream& operator<<(std::ostream&s, const weights& rhs)
	{
		s << "weights(sizes=" << rhs.size() << "):\n";
		int l=0;
		for(auto i: rhs._dat) {
			if (i!=0) {
				s << "\tweight(" << ++l << ", size=" << i->size() << "):\n";
				for(auto j: *i) {
					s << "\t\t" << *j << "\n";
				}	
			}
		}
		return s << "\n";
	}
};
