#include "mlp.h"
#include <sstream>
#include <cuda_runtime.h>

typedef float t_flt;

template<typename T> std::string prettyprint(const T t)
{
	std::ostringstream s;
	s << (t>=0 ? " " : "") << boost::format("%2.6f") % t;
	return s.str();
}

__global__ void cuda_hello()
{
	printf("Hello World from GPU!\n");
}

int main(int argc, char* argv[])
{
	(void)argc;
	(void)argv;
	
	const int M=8;	
	// prepare XOR traing/test data
	const t_flt X[M][3]={
				{0,	0, 0},
				{0,	0, 1},
				{0,	1, 0},
				{0,	1, 1},
				{1,	0, 0},
				{1,	0, 1},
				{1,	1, 0},
				{1,	1, 1}, 
	};
	
	const t_flt y[M][1]={
				{0},
				{1},
				{1},
				{0},
				{1},
				{0},
				{0},
				{1} 
	};
	
	// defining a net with 4 layers having 3,3,2, and 1 neuron respectively,
	// the first layer is input layer i.e. simply holder for the input parameters
	// and has to be the same size as the no of input parameters, in out example 3
	const int numLayers=4, lSz[4]={3, 3, 2 ,1};

	MLP<activation_functions::ELU, weight_initializers::He> model(numLayers, lSz, 0.1, 1, 0.1, 200000, 0.00001);
	
	cout << "Fit.." << endl;	
	model.fit(M, 3, X, y);
	
	cout << "Predict.." << endl;	
	for(int i=0; i<M; i++ ) {
		model.predict(X[i]);
		const t_flt y_pred = model.Out(0), y_true=y[i][0], diff=y_pred-y_true;
		cout << "  X[" << i << "]=[" << X[i][0] << ", " << X[i][1]<< ", "  << X[i][2] << "],  ";
		cout << "y_pred=" << prettyprint(y_pred) << ",  y_true=" << y_true << ",  diff=" << prettyprint(diff) <<  endl;
	}
	cout << model << endl;
	//model.printweights();
	cuda_hello<<<1,1>>>(); 

	return 0;
}
