import libmlpsimple as lib

import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

def TestLib():	
	layersizes = np.array([3, 3, 2 ,1], dtype=np.int32)
	model = lib.mlpsimple(layersizes, 0.03, 1, 0.1, 2000000, 0.00001)

	#x = np.zeros([4, 6])
	#print(model.test(x))
	#print(model.dump())

	X_train = np.array([
			[0,  0,  0],
			[0,  0,  1],
			[0,  1,  0],
			[0,  1,  1],
			[1,  0,  0],
			[1,  0,  1],
			[1,  1,  0],
			[1,  1,  1]
		], dtype=np.float32)
		
	y_train = np.array([
			[0],
			[1],
			[1],
			[0],
			[1],
			[0],
			[0],
			[1]
		], dtype=np.float32)

	X_test = np.array([
			[0,  0,  0],
			[0,  0,  1],
			[0,  1,  0],
			[0,  1,  1],
			[1,  0,  0],
			[1,  0,  1],
			[1,  1,  0],
			[1,  1,  1]
		], dtype=np.float32)

	model.fit(X_train, y_train)
	y_pred = model.predict(X_test)
	print(f"y_pred={y_pred}")

def TestFitPredict(plot=True):
	import seaborn as sns
	from sklearn.linear_model import LinearRegression
	
	iris = sns.load_dataset('iris')
	X_iris = iris.drop('species', axis = 1)
	X_iris.shape
	y_iris = iris['species']
	y_iris.shape

	rng = np.random.RandomState(35)
	x = 10*rng.rand(40)
	y = 2*x-1+rng.randn(40)
	plt.scatter(x,y);
	
	model = LinearRegression(fit_intercept=True)
	X = x[:, np.newaxis]
	X.shape

	model.fit(X, y)
	print(f"model.coef_     ={model.coef_}")
	print(f"model.intercept_={model.intercept_}")
	print(f"model.score(X,y)={model.score(X, y)}") 

	x_test = np.linspace(-1, 11)
	X_test = x_test[:, np.newaxis]
	y_pred = model.predict(X_test)
	print(f"model.score(X_test, y_pred)={model.score(X_test, y_pred)}") 

	if plot:
		plt.scatter(x, y)
		plt.plot(x_test, y_pred)
		plt.show()

def TestMLP():	
	from matplotlib.colors import ListedColormap
	from sklearn.datasets import make_moons, make_circles, make_classification
	
	#iris = datasets.load_iris()
	#X = iris.data[:, [0, 2]]
	#y = iris.target
	X, y1 = make_moons(n_samples=1000, noise=0.1, random_state=0)
	
	X = X.astype(np.float32)
	y1 = y1.astype(np.float32)
	M = y1.shape[0]
	y = np.zeros((M, 1), dtype=np.float32)
	for i in range(M):
		y[i]=y1[i]
		
	libmode=True
	if not libmode:
		y = y[:,0]
	
	X = StandardScaler().fit_transform(X)
	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.2, random_state=1)

	if libmode:
		model = lib.mlpsimple(np.array([2, 100, 10, 1], dtype=np.int32), 0.0003, 1, 0.2, 200000, 0.00001)
	else:
		model = MLPClassifier(hidden_layer_sizes=[100, 100], activation='logistic', solver='sgd',  learning_rate='constant', learning_rate_init=0.1, max_iter=20000,  alpha=0.1, random_state=2)
		model = MLPClassifier(hidden_layer_sizes=[100, 100], activation='relu',     solver='lbfgs',learning_rate='constant', learning_rate_init=0.1, max_iter=20000,  alpha=0.1, random_state=2)
	
	#print(f"X_train=\n{X_train}")
	#print(f"y_trainx=\n{y_train}")
	print(f"X_train.shape={X_train.shape}")
	print(f"y_train.shape={y_train.shape}")

	model.fit(X_train, y_train)
	
	def PlotDecisionRegions(clf):
		h = .02  # step size in the mesh
		
		x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
		y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
		xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

		# just plot the dataset first
		cm = plt.cm.RdBu
		cm_bright = ListedColormap(['#FF0000', '#0000FF'])
		ax = plt # plt.subplot(len(datasets), len(classifiers) + 1, i)
		# Plot the training points

		if libmode:
			#yz = y_train[:,0]
			#y_train = yz
			#yz = y_test[:,0]
			#y_test = yz
			pass

		plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train[:,0], cmap=cm_bright)
		# and testing points
		ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test[:,0], cmap=cm_bright, alpha=0.3)
		
		#ax.set_xlim(xx.min(), xx.max())
		#ax.set_ylim(yy.min(), yy.max())
		#ax.set_xticks(())
		#ax.set_yticks(())
		# Plot the decision boundary. For that, we will assign a color to each
		# point in the mesh [x_min, x_max]x[y_min, y_max].
		if hasattr(clf, "decision_function"):
			Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
		else:
			#Z = clf.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1]
			Xp = np.c_[xx.ravel(), yy.ravel()]
			Xp = Xp.astype(np.float32)

			Z = clf.predict(Xp)#[:, 1]
			if  libmode:
				Z = Z[:,0]

		# Put the result into a color plot
		Z = Z.reshape(xx.shape)
		ax.contourf(xx, yy, Z, cmap=cm, alpha=.8)

		# Plot also the training points
		ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train[:,0], cmap=cm_bright,            edgecolors='black', s=15)
		# and testing points
		ax.scatter(X_test[:, 0], X_test[:, 1],   c=y_test[:,0],  cmap=cm_bright, alpha=0.3, edgecolors='black', s=15)

		#ax.set_xlim(xx.min(), xx.max())
		#ax.set_ylim(yy.min(), yy.max())
		#ax.set_xticks(())
		#ax.set_yticks(())
		#ax.set_title(name)
		#ax.text(xx.max() - .3, yy.min() + .3, ('%.2f' % score).lstrip('0'), size=15, horizontalalignment='right')		
		
		plt.show()
	
	PlotDecisionRegions(model)

def main():
	print("PYTHON: init..")
	#TestLib()
	#TestFitPredict()
	TestMLP()
	print("PYTHON: done..")

if __name__=="__main__":
	main()